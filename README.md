**Form Bot**

An demonstration for using Angular forms with Servo. Includes: 
* Angular service for communicating with Servo
* Submitting data fields from Angular to Servo
* Receiving and presenting fields from Servo

---

## Install

1. clone/download this repo
2. run npm install
3. install  Servo 
4. copy form-steps folder from convocode/common/getting-started/trees to convocode/anonymous/drafts/form-steps
5. Run Servo 

---

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

